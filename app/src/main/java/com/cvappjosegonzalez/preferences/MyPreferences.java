package com.cvappjosegonzalez.preferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Artacho on 20/05/14.
 */
public class MyPreferences {

    private static final String TAG = "MyPreferences";
    private static final String KEY_PREFERENCES = "myPreferences";

    private static final String KEY_LANGUAGE = "language";
    private static final String KEY_JSON = "jsonName";

    private SharedPreferences mSharedPreferences;

    public MyPreferences(Context context) {
        mSharedPreferences = context.
                getSharedPreferences(KEY_PREFERENCES, Context.MODE_PRIVATE);
    }

    public String getLanguage() {
        return mSharedPreferences.getString(KEY_LANGUAGE, null);
    }

    public void setLanguage(String language) {
        mSharedPreferences.edit().putString(KEY_LANGUAGE, language).commit();
    }

    public String getJson() {
        return mSharedPreferences.getString(KEY_JSON, null);
    }

    public void setJson(String jsonName) {
        mSharedPreferences.edit().putString(KEY_JSON, jsonName).commit();
    }
}
