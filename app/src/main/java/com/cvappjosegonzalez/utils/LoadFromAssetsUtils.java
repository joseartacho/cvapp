package com.cvappjosegonzalez.utils;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Artacho on 20/05/14.
 */
public class LoadFromAssetsUtils {
    private static final String TAG = "DashBoardActivity";

    public static String loadJSONFromAsset(Context context, String jsonFileName) {
        String json;
        try {
            InputStream is = context.getResources().getAssets().open(jsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            Log.d(TAG, json);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
