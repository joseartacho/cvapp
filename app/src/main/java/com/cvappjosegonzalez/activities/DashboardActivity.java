package com.cvappjosegonzalez.activities;


import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;

import com.cvappjosegonzalez.adapters.FragmentAdapter;
import com.cvappjosegonzalez.data.CvJson;
import com.cvappjosegonzalez.fragments.ComplementaryFragment;
import com.cvappjosegonzalez.fragments.ContactFragment;
import com.cvappjosegonzalez.fragments.EducationFragment;
import com.cvappjosegonzalez.fragments.ExperienceFragment;
import com.cvappjosegonzalez.fragments.NavigationDrawerFragment;
import com.cvappjosegonzalez.fragments.PersonalDataFragment;
import com.cvappjosegonzalez.fragments.SkillsFragment;
import com.cvappjosegonzalez.preferences.MyPreferences;
import com.cvappjosegonzlez.app.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class DashboardActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private static final String TAG = "DashBoardActivity";
    private static final String TAG_SECTION_SELECTED = "SectionSelected";

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private String mTitle;
    private int mSectionSelected = -1;
    private FragmentAdapter mAdapter;
    private ViewPager mPager;
    private List<CvJson> mCv;
    private MyPreferences mMyPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);


        Type myTypeToken = new TypeToken<List<CvJson>>() {}.getType();
        mMyPreferences = new MyPreferences(this);

        Gson gson = new Gson();
        mCv = gson.fromJson(mMyPreferences.getJson(), myTypeToken);

        mTitle = mCv.get(0).getCategory();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        Type myTypeToken = new TypeToken<List<CvJson>>() {}.getType();
        mMyPreferences = new MyPreferences(this);

        Gson gson = new Gson();
        mCv = gson.fromJson(mMyPreferences.getJson(), myTypeToken);
        Fragment fragment= null;
        switch (position) {
            case 0:
                fragment = new PersonalDataFragment();
                mTitle = mCv.get(0).getCategory();
                break;
            case 1:
                fragment = new ExperienceFragment();
                mTitle = mCv.get(1).getCategory();
                break;
            case 2:
                fragment = new EducationFragment();
                mTitle = mCv.get(2).getCategory();
                break;
            case 3:
                fragment = new ComplementaryFragment();
                mTitle = mCv.get(3).getCategory();
                break;
            case 4:
                fragment = new SkillsFragment();
              // mPager = (ViewPager) findViewById(R.id.pager);
                //mPager.setAdapter(new FragmentAdapter(getSupportFragmentManager()));
                mTitle = mCv.get(4).getCategory();
                break;
            case 5:
                fragment = new ContactFragment();
                mTitle = mCv.get(5).getCategory();
                break;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, 0)
                .replace(R.id.container, fragment)
                .commit();

    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.dashboard, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null) {
            mSectionSelected = savedInstanceState
                    .getInt(TAG_SECTION_SELECTED);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putInt(TAG_SECTION_SELECTED, mSectionSelected);
    }

}
