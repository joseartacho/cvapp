package com.cvappjosegonzalez.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.cvappjosegonzalez.fragments.SkillsFragment;

/**
 * Created by Artacho on 21/05/14.
 */
public class FragmentAdapter extends FragmentPagerAdapter {

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position)
    {
        Fragment fragment = null;

        switch(position){
            case 0:
                fragment = new SkillsFragment();
                break;

            case 1:
                fragment = new SkillsFragment();
                break;
        }

        return fragment;
    }


    @Override
    public int getCount() {
        return 2;
    }

}