package com.cvappjosegonzalez.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Artacho on 20/05/14.
 */
public class CvJson {
    @SerializedName("id")
    private int mId;

    @SerializedName("category")
    private String mCategory;

    @SerializedName("information")
    private List<Cv> mCvJson;

    public int getId() {
        return mId;
    }

    public String getCategory() {
        return mCategory;
    }

    public List<Cv> getCvJson() {
        return mCvJson;
    }

}

