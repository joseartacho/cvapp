package com.cvappjosegonzalez.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Artacho on 20/05/14.
 */
public class Cv {
    @SerializedName("date")
    private String mDate;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("content")
    private String mContent;

    @SerializedName("link")
    private String mLink;

    public String getDate() {
        return mDate;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getContent() {
        return mContent;
    }

    public String getLink() {
        return mLink;
    }
}
