package com.cvappjosegonzalez.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.cvappjosegonzalez.activities.DashboardActivity;
import com.cvappjosegonzalez.preferences.MyPreferences;
import com.cvappjosegonzalez.utils.LoadFromAssetsUtils;
import com.cvappjosegonzlez.app.R;

/**
 * Created by Artacho on 21/05/14.
 */
public class LanguageFragment extends Fragment{

    private Button mEnglandButton;
    private Button mSpainButton;
    private final static String ENGLISH_JSON_FILE_NAME = "cvenglish.json";
    private final static String SPANISH_JSON_FILE_NAME = "cvspanish.json";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_language, container, false);

        mEnglandButton = (Button) view.findViewById(R.id.english);
        mSpainButton = (Button) view.findViewById(R.id.spanish);


        mEnglandButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MyPreferences myPreferences =
                        new MyPreferences(getActivity());
                myPreferences.setJson(LoadFromAssetsUtils.loadJSONFromAsset(getActivity(), ENGLISH_JSON_FILE_NAME));
                Intent intent = new Intent(getActivity(), DashboardActivity.class);
                startActivity(intent);
            }
        });

        mSpainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MyPreferences myPreferences =
                        new MyPreferences(getActivity());
                myPreferences.setJson(LoadFromAssetsUtils.loadJSONFromAsset(getActivity(), SPANISH_JSON_FILE_NAME));
                Intent intent = new Intent(getActivity(), DashboardActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }
}
