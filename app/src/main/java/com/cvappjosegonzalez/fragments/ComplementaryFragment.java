package com.cvappjosegonzalez.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cvappjosegonzalez.data.CvJson;
import com.cvappjosegonzalez.preferences.MyPreferences;
import com.cvappjosegonzlez.app.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Artacho on 20/05/14.
 */
public class ComplementaryFragment extends Fragment {

    private TextView mDateComplementary[] = new TextView[5];
    private TextView mSectionComplementary[] = new TextView[5];
    private TextView mContentComplementary[] = new TextView[5];

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_complementary, container, false);

        mDateComplementary[0] = (TextView) view.findViewById(R.id.date1_complementary);
        mDateComplementary[1] = (TextView) view.findViewById(R.id.date2_complementary);
        mDateComplementary[2] = (TextView) view.findViewById(R.id.date3_complementary);
        mDateComplementary[3] = (TextView) view.findViewById(R.id.date4_complementary);
        mDateComplementary[4] = (TextView) view.findViewById(R.id.date5_complementary);
        mSectionComplementary[0] = (TextView) view.findViewById(R.id.section1_complementary);
        mSectionComplementary[1] = (TextView) view.findViewById(R.id.section2_complementary);
        mSectionComplementary[2] = (TextView) view.findViewById(R.id.section3_complementary);
        mSectionComplementary[3] = (TextView) view.findViewById(R.id.section4_complementary);
        mSectionComplementary[4] = (TextView) view.findViewById(R.id.section5_complementary);
        mContentComplementary[0] = (TextView) view.findViewById(R.id.content1_complementary);
        mContentComplementary[1] = (TextView) view.findViewById(R.id.content2_complementary);
        mContentComplementary[2] = (TextView) view.findViewById(R.id.content3_complementary);
        mContentComplementary[3] = (TextView) view.findViewById(R.id.content4_complementary);
        mContentComplementary[4] = (TextView) view.findViewById(R.id.content5_complementary);

        Type myTypeToken = new TypeToken<List<CvJson>>() {}.getType();
        MyPreferences myPreferences = new MyPreferences(
                getActivity());

        Gson gson = new Gson();
        List<CvJson> mCv = gson
                .fromJson(myPreferences.getJson(),
                        myTypeToken);

        for (int i = 0; i<5 ; i++) {
            mDateComplementary[i].setText(mCv.get(3).getCvJson().get(i).getDate());
            mSectionComplementary[i].setText(mCv.get(3).getCvJson().get(i).getTitle());
            mContentComplementary[i].setText(mCv.get(3).getCvJson().get(i).getContent());
        }
        return view;
    }
}
