package com.cvappjosegonzalez.fragments;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cvappjosegonzalez.data.CvJson;
import com.cvappjosegonzalez.preferences.MyPreferences;
import com.cvappjosegonzalez.utils.LoadFromAssetsUtils;
import com.cvappjosegonzlez.app.R;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Artacho on 20/05/14.
 */
public class PersonalDataFragment extends Fragment {

    private TextView mSection[] = new TextView[5];
    private TextView mContent[] = new TextView[5];

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_personal_data, container, false);


        mSection[0] = (TextView) view.findViewById(R.id.section1_personal_data);
        mSection[1] = (TextView) view.findViewById(R.id.section2_personal_data);
        mSection[2] = (TextView) view.findViewById(R.id.section3_personal_data);
        mSection[3] = (TextView) view.findViewById(R.id.section4_personal_data);
        mSection[4] = (TextView) view.findViewById(R.id.section5_personal_data);
        mContent[0] = (TextView) view.findViewById(R.id.content1_personal_data);
        mContent[1] = (TextView) view.findViewById(R.id.content2_personal_data);
        mContent[2] = (TextView) view.findViewById(R.id.content3_personal_data);
        mContent[3] = (TextView) view.findViewById(R.id.content4_personal_data);
        mContent[4] = (TextView) view.findViewById(R.id.content5_personal_data);

        Type myTypeToken = new TypeToken<List<CvJson>>() {}.getType();
        MyPreferences myPreferences = new MyPreferences(
                getActivity());

        Gson gson = new Gson();
        List<CvJson> mCv = gson
                .fromJson(myPreferences.getJson(),
                        myTypeToken);

        //Google Analytics
        Tracker easyTracker = EasyTracker.getInstance(getActivity());
        easyTracker.set(Fields.SCREEN_NAME, "Personal Data");
        easyTracker.send(MapBuilder
                        .createAppView()
                        .build()
        );

        for (int i = 0; i<5 ; i++) {
            mSection[i].setText(mCv.get(0).getCvJson().get(i).getTitle());
            mContent[i].setText(mCv.get(0).getCvJson().get(i).getContent());
        }
        return view;
    }
}
