package com.cvappjosegonzalez.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.cvappjosegonzalez.data.CvJson;
import com.cvappjosegonzalez.preferences.MyPreferences;
import com.cvappjosegonzlez.app.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Artacho on 20/05/14.
 */
public class EducationFragment extends Fragment {

    private TextView mDateEducation[] = new TextView[2];
    private TextView mInstitution[] = new TextView[2];
    private TextView mContentEducation[] = new TextView[2];
    private Button mMoreInfo[] = new Button[2];
    private String mLink[] = new String[2];

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_education, container, false);

        mDateEducation[0] = (TextView) view.findViewById(R.id.date1_education);
        mDateEducation[1] = (TextView) view.findViewById(R.id.date2_education);
        mInstitution[0] = (TextView) view.findViewById(R.id.section1_education);
        mInstitution[1] = (TextView) view.findViewById(R.id.section2_education);
        mContentEducation[0] = (TextView) view.findViewById(R.id.content1_education);
        mContentEducation[1] = (TextView) view.findViewById(R.id.content2_education);
        mMoreInfo[0] = (Button) view.findViewById(R.id.more_info1_education);
        mMoreInfo[1] = (Button) view.findViewById(R.id.more_info2_education);

        Type myTypeToken = new TypeToken<List<CvJson>>() {}.getType();
        MyPreferences myPreferences = new MyPreferences(
                getActivity());

        Gson gson = new Gson();
        List<CvJson> mCv = gson
                .fromJson(myPreferences.getJson(),
                        myTypeToken);

        for (int i = 0; i<2 ; i++) {
            mDateEducation[i].setText(mCv.get(2).getCvJson().get(i).getDate());
            mInstitution[i].setText(mCv.get(2).getCvJson().get(i).getTitle());
            mContentEducation[i].setText(mCv.get(2).getCvJson().get(i).getContent());
            mLink[i] = (mCv.get(2).getCvJson().get(i).getLink());

            mMoreInfo[i].setOnClickListener(
                    new mMoreInfoListener(mLink[i]));
        }


        return view;
    }

    private class mMoreInfoListener implements  View.OnClickListener {
        private String mClickedLink;
        private mMoreInfoListener(String link) {
            mClickedLink = link;
        }
        @Override
        public void onClick(View v) {
            Intent intent = null;
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mClickedLink));
            startActivity(intent);
        }
    }
}
