package com.cvappjosegonzalez.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.cvappjosegonzalez.data.CvJson;
import com.cvappjosegonzalez.dialogs.CallDialogFragment;
import com.cvappjosegonzalez.preferences.MyPreferences;
import com.cvappjosegonzlez.app.R;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Artacho on 20/05/14.
 */
public class ContactFragment extends Fragment {

    private TextView mContent[] = new TextView[2];
    private Button mMoreInfoContact;
    private String mLinkContact;
    private String mCallTitle;
    private List<CvJson> mCv;

    private static final String TAG = "PersonalDataFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_contact, container, false);

        mContent[0] = (TextView) view.findViewById(R.id.content1_contact);
        mContent[1] = (TextView) view.findViewById(R.id.content2_contact);
        mMoreInfoContact = (Button) view.findViewById(R.id.more_info1_contact);

        Type myTypeToken = new TypeToken<List<CvJson>>() {}.getType();
        MyPreferences myPreferences = new MyPreferences(
                getActivity());

        Gson gson = new Gson();
        mCv = gson
                .fromJson(myPreferences.getJson(),
                        myTypeToken);

        mContent[0].setText(mCv.get(5).getCvJson().get(0).getContent());
        mContent[1].setText(mCv.get(5).getCvJson().get(1).getContent());
        mLinkContact = mCv.get(5).getCvJson().get(2).getLink();

        Tracker easyTracker = EasyTracker.getInstance(getActivity());
        easyTracker.set(Fields.SCREEN_NAME, "Contact");
        easyTracker.send(MapBuilder
                        .createAppView()
                        .build()
        );

        Log.d(TAG, mLinkContact);

        mMoreInfoContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EasyTracker easyTracker = EasyTracker.getInstance(getActivity());
                easyTracker.send(MapBuilder
                                .createEvent("Contact",     // Event category (required)
                                        "LinkedIn Button",  // Event action (required)
                                        null,   // Event label
                                        null)       // Event value
                                .build()
                );
                Intent intent = null;
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mLinkContact));
                startActivity(intent);
            }
        });

        mContent[0].setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                mCallTitle = mCv.get(5).getCvJson().get(0).getTitle();
                CallDialogFragment callDialogFragment
                        = new CallDialogFragment(null, mCallTitle);
                callDialogFragment.show(getFragmentManager(), TAG);
            }
        });

        mContent[1].setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("mailto:" + mCv.get(5).getCvJson().get(1).getContent()));
                intent.putExtra(Intent.EXTRA_SUBJECT,"");
                intent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(intent);
            }
        });

        return view;
    }
}
