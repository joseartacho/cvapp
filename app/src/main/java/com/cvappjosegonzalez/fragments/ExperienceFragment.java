package com.cvappjosegonzalez.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.cvappjosegonzalez.data.CvJson;
import com.cvappjosegonzalez.preferences.MyPreferences;
import com.cvappjosegonzlez.app.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Artacho on 20/05/14.
 */
public class ExperienceFragment extends Fragment {

    private TextView mDateExperience;
    private TextView mTitleExperience;
    private TextView mContentExperience;
    private Button mMoreInfoExperience;
    private String mLinkExperience;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_experience, container, false);

        mDateExperience = (TextView) view.findViewById(R.id.date1_experience);
        mTitleExperience = (TextView) view.findViewById(R.id.section1_experience);
        mContentExperience = (TextView) view.findViewById(R.id.content1_experience);
        mMoreInfoExperience = (Button) view.findViewById(R.id.more_info1_experience);

        Type myTypeToken = new TypeToken<List<CvJson>>() {}.getType();
        MyPreferences myPreferences = new MyPreferences(
                getActivity());

        Gson gson = new Gson();
        List<CvJson> mCv = gson
                .fromJson(myPreferences.getJson(),
                        myTypeToken);

        mDateExperience.setText(mCv.get(1).getCvJson().get(0).getDate());
        mTitleExperience.setText(mCv.get(1).getCvJson().get(0).getTitle());
        mContentExperience.setText(mCv.get(1).getCvJson().get(0).getContent());
        mLinkExperience = (mCv.get(1).getCvJson().get(0).getLink());

        mMoreInfoExperience.setOnClickListener(
                new mMoreInfoListener(mLinkExperience));

        return view;
    }

    private class mMoreInfoListener implements  View.OnClickListener {
        private String mClickedLink;
        private mMoreInfoListener(String link) {
            mClickedLink = link;
        }
        @Override
        public void onClick(View v) {
            Intent intent = null;
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mClickedLink));
            startActivity(intent);
        }
    }
}
