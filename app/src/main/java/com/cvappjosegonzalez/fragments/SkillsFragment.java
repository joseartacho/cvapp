package com.cvappjosegonzalez.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cvappjosegonzalez.data.CvJson;
import com.cvappjosegonzalez.preferences.MyPreferences;
import com.cvappjosegonzlez.app.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Artacho on 20/05/14.
 */
public class SkillsFragment extends Fragment {

    private TextView mSection[] = new TextView[5];
    private TextView mContent[] = new TextView[5];
    private static final String TAG = "PersonalDataFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_skills, container, false);


        mSection[0] = (TextView) view.findViewById(R.id.section1_skills);
        mSection[1] = (TextView) view.findViewById(R.id.section2_skills);
        mSection[2] = (TextView) view.findViewById(R.id.section3_skills);
        mSection[3] = (TextView) view.findViewById(R.id.section4_skills);
        mSection[4] = (TextView) view.findViewById(R.id.section5_skills);
        mContent[0] = (TextView) view.findViewById(R.id.content1_skills);
        mContent[1] = (TextView) view.findViewById(R.id.content2_skills);
        mContent[2] = (TextView) view.findViewById(R.id.content3_skills);
        mContent[3] = (TextView) view.findViewById(R.id.content4_skills);
        mContent[4] = (TextView) view.findViewById(R.id.content5_skills);

        Type myTypeToken = new TypeToken<List<CvJson>>() {}.getType();
        MyPreferences myPreferences = new MyPreferences(
                getActivity());

        Gson gson = new Gson();
        List<CvJson> mCv = gson
                .fromJson(myPreferences.getJson(),
                        myTypeToken);

        for (int i = 0; i<5 ; i++) {
            mSection[i].setText(mCv.get(4).getCvJson().get(i).getTitle());
            mContent[i].setText(mCv.get(4).getCvJson().get(i).getContent());
        }
        return view;
    }
}
