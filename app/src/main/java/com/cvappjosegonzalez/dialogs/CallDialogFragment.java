package com.cvappjosegonzalez.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.cvappjosegonzlez.app.R;


public class CallDialogFragment extends DialogFragment {

    private static final String KEY_TITLE = "title";

    private String mAlertTitle;
    private String mAlertContent;

    public CallDialogFragment() {
        super();
    }

    public CallDialogFragment(String mCallTitle, String mCallContent) {
        mAlertTitle = mCallTitle;
        mAlertContent = mCallContent;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mAlertTitle = savedInstanceState.getString(KEY_TITLE);
        }
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(mAlertTitle);
        builder.setMessage(mAlertContent)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked CALL button
                        startDialActivity(getResources().getString(R.string.phone));
                    }
                })
                .setNegativeButton(R.string.no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int id) {
                                // User cancelled the dialog
                            }
                        });

        // Create the AlertDialog object and return it
        return builder.create();
    }

    private void startDialActivity(String phone){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phone));
        startActivity(intent);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_TITLE, mAlertTitle);
    }
}
